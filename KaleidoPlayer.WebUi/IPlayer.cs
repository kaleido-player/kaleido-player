﻿namespace KaleidoPlayer.WebUi
{
    public interface IPlayer
    {
        public void Play(string mrl);
        public void ToggleFullscreen();
    }
}
