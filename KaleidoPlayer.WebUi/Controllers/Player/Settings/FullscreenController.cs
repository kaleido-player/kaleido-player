﻿using Microsoft.AspNetCore.Mvc;

namespace KaleidoPlayer.WebUi.Controllers.Player.Settings
{
    [ApiController]
    [Route("api/player/settings/fullscreen")]
    public sealed class FullscreenController
    {
        private readonly IPlayer _player;

        public FullscreenController(IPlayer player)
        {
            _player = player;
        }

        [HttpPost]
        // ReSharper disable once UnusedMember.Global
        public void Post()
        {
            _player.ToggleFullscreen();
        }
    }
}
