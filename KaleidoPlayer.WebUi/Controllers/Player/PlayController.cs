﻿using Microsoft.AspNetCore.Mvc;

namespace KaleidoPlayer.WebUi.Controllers.Player
{
    [ApiController]
    [Route("api/player/[controller]")]
    public sealed class PlayController
    {
        private readonly IPlayer _player;

        public PlayController(IPlayer player)
        {
            _player = player;
        }

        [HttpPost]
        // ReSharper disable once UnusedMember.Global
        public void Post(PlayPost data)
        {
            _player.Play(data.Stream);
        }
        
        // ReSharper disable once ClassNeverInstantiated.Global
        public sealed class PlayPost
        {
            public PlayPost(string stream)
            {
                Stream = stream;
            }

            public string Stream { get; }
        }
    }
}
