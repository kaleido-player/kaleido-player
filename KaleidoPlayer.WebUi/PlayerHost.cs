﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace KaleidoPlayer.WebUi
{
    public sealed class PlayerHost
    {
        private readonly IHost _host;

        public PlayerHost(IPlayer player)
        {
            _host = Host.CreateDefaultBuilder()
                .ConfigureServices(services => services.AddSingleton(player))
                .ConfigureWebHostDefaults(builder => builder.UseStartup<Startup>())
                .Build();
        }

        public void Start()
        {
            _host.Start();
        }

        internal static void Main()
        {
        }
    }
}
