﻿using System.Windows.Forms;

namespace KaleidoPlayer.WinForms.Player.Settings
{
    public interface IFullscreenState
    {
        IFullscreenState Toggle(Form form);

        static IFullscreenState Initial()
        {
            return new OffState();
        }

        private sealed class OnState : IFullscreenState
        {
            private readonly WindowSettings _previous;

            public OnState(WindowSettings previous)
            {
                _previous = previous;
            }

            public IFullscreenState Toggle(Form form)
            {
                _previous.Set(form);
                return new OffState();
            }
        }

        private sealed class OffState : IFullscreenState
        {
            public IFullscreenState Toggle(Form form)
            {
                var previous = new WindowSettings(form);

                WindowSettings.Fullscreen.Set(form);

                return new OnState(previous);
            }
        }

        sealed class WindowSettings
        {
            private readonly FormWindowState _state;
            private readonly FormBorderStyle _style;

            public WindowSettings(Form form) : this(form.FormBorderStyle, form.WindowState)
            {
            }

            private WindowSettings(FormBorderStyle style, FormWindowState state)
            {
                _style = style;
                _state = state;
            }

            public static WindowSettings Fullscreen { get; } = new(FormBorderStyle.None, FormWindowState.Maximized);

            public void Set(Form form)
            {
                form.FormBorderStyle = _style;
                form.WindowState = _state;
            }
        }
    }
}
