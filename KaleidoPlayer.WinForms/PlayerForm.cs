﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using KaleidoPlayer.WebUi;
using KaleidoPlayer.WinForms.Player.Settings;
using LibVLCSharp.Shared;
using LibVLCSharp.WinForms;

namespace KaleidoPlayer.WinForms
{
    public sealed class PlayerForm : Form, IPlayer
    {
        private readonly LibVLC _libVlc;
        private readonly MediaPlayer _player;
        private readonly VideoView _videoView;
        private IFullscreenState _fullscreenState = IFullscreenState.Initial();


        public PlayerForm()
        {
            if (!DesignMode) Core.Initialize();

            _videoView = new VideoView();
            ((ISupportInitialize) _videoView).BeginInit();
            SuspendLayout();

            _videoView.BackColor = Color.Black;
            _videoView.Dock = DockStyle.Fill;
            _videoView.Name = "_videoView";

            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Text = "KaleidoPlayer";
            Controls.Add(_videoView);

            ((ISupportInitialize) _videoView).EndInit();

            ResumeLayout();

            _libVlc = new LibVLC();
            _player = new MediaPlayer(_libVlc);
            _videoView.MediaPlayer = _player;
        }

        void IPlayer.Play(string mrl)
        {
            _player.Play(new Media(_libVlc, mrl, FromType.FromLocation));
        }

        void IPlayer.ToggleFullscreen()
        {
            Invoke(() => { _fullscreenState = _fullscreenState.Toggle(this); });
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            new PlayerHost(this).Start();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _videoView.Dispose();

            base.Dispose(disposing);
        }
    }
}
