using System;
using System.Windows.Forms;

namespace KaleidoPlayer.WinForms
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.Run(new PlayerForm());
        }
    }
}
